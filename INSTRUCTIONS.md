# Desafio Backend QRPOINT

## ➤ Getting Started

Spend a minute reading this getting started guide to setup and run the API correctly.

### Installation

To install the API run the following command on your terminal:

```sh
yarn
```

or

```sh
npm install
```

### Run the app

Run the command

```sh
yarn dev
```

and go to  `http://localhost:8000` endpoint.

### Run tests

To run tests, type the following command on your terminal:

```sh
yarn test
```

---

## ➤ REST API

### Use a **Postgres** instance with Docker

> Use the postgres:11 image instead

```sh
docker run --name some-postgres-name -e POSTGRES_PASSWORD=mysecretpassword -p 5432:5432 -d postgres:11
```

### Features

- RESTful routing
- Models with proper relationships
- Controllers/Models etc with proper separation of concerns
- JWT Authentication
- Docker images

---

### Requests

### Users

> Create an admin user that will be responsible for registrations (create members, employers, data and licenses)

#### POST

``POST http://localhost:8000/users``

```json
{
  "name": "admin",
  "email": "admin@email.com",
  "password": "123456",
}
```

---
### Session

> Creating a new session (JWT authentication)

#### POST

``POST http://localhost:8000/sessions``

```json
{
  "email": "admin@email.com",
  "password": "123456",
}
```
---

### Employers

> Creating and listing employers

#### POST

*Provide Bearer Token returned from user session for authentication*

``POST http://localhost:8000/employers``

```json
{
  "employer_name": "Academia Jedi",
  "employer_code": "#123",
  "thumbnail": "image.jpg"
}
```

#### GET

*Provide Bearer Token returned from user session for authentication*

``GET http://localhost:8000/employers/{employerId}``

---

### Members

#### POST

> Creating and listing the members

*Provide Bearer Token returned from user session for authentication*

``POST http://localhost:8000/members``

```json
{
  "member_name": "John Doe",
  "birthday": "01-01-1990",
  "pin_code": 123456,
  "employer_id": 1
}
```

#### GET

*Provide Bearer Token returned from user session for authentication*

``GET http://localhost:8000/members/{memberId}``



### Member Data

> Creating the member aditional data

#### POST

*Provide Bearer Token returned from user session for authentication*

``POST http://localhost:8000/data``

```json
{
  "father": "john father",
  "mother": "mary mother",
  "member_id": 2
}
```

---

### Medical Licenses
> Creating and listing the medical licenses


#### POST

*Provide Bearer Token returned from user session for authentication*

``POST http://localhost:8000/submit-medical-licenses``

```json
{
  "initial_date": "01-01-2019",
  "final_date": "02-01-2019",
  "time": 23,
  "member_id": 1
}
```

#### GET

*Provide Bearer Token returned from user session for authentication*

``GET http://localhost:8000/medical-licenses``
