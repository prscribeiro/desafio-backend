import * as Yup from 'yup';
import Employer from '../models/Employer';

class MessageController {
  async store(req, res) {
    const schema = Yup.object().shape({
      employer_name: Yup.string().required(),
      employer_code: Yup.string().required(),
      thumbnail: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res
        .status(400)
        .json({ error: 'Please, insert valid information' });
    }

    const { employer_name, employer_code, thumbnail } = req.body;
    const employer = await Employer.create({
      employer_name,
      employer_code,
      thumbnail,
    });

    return res.json(employer);
  }

  async index(req, res) {
    const employers = await Employer.findAll({
      where: {
        id: req.params.employerId,
      },
      order: ['created_at'],
    });
    return res.json(employers);
  }
}

export default new MessageController();
