import License from '../models/License';
import Member from '../models/Member';

class LicenseController {
  async store(req, res) {
    const { initial_date, final_date, member_id, time } = req.body;
    const license = await License.create({
      initial_date,
      final_date,
      member_id,
      time,
    });

    return res.json(license);
  }

  async index(req, res) {
    const licenses = await License.findAll({
      attributes: ['id', 'initial_date', 'final_date', 'time'],
      include: [{ model: Member, as: 'member', attributes: ['member_name'] }],
    });
    return res.json(licenses);
  }
}

export default new LicenseController();
