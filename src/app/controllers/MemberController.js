import * as Yup from 'yup';
import Member from '../models/Member';

class MemberController {
  async store(req, res) {
    const schema = Yup.object().shape({
      member_name: Yup.string().required(),
      birthday: Yup.string().required(),
      pin_code: Yup.number().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res
        .status(400)
        .json({ error: 'Please, insert valid information' });
    }

    const { member_name, birthday, pin_code, employer_id } = req.body;
    const member = await Member.create({
      member_name,
      birthday,
      pin_code,
      employer_id,
    });

    return res.json(member);
  }

  async index(req, res) {
    const members = await Member.findAll({
      where: {
        id: req.params.memberId,
      },
      order: ['created_at'],
    });
    return res.json(members);
  }
}

export default new MemberController();
