import * as Yup from 'yup';
import Data from '../models/Data';

class DataController {
  async store(req, res) {
    const schema = Yup.object().shape({
      father: Yup.string().required(),
      mother: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res
        .status(400)
        .json({ error: 'Please, insert valid information' });
    }

    const { father, mother, member_id } = req.body;
    const data = await Data.create({
      father,
      mother,
      member_id,
    });

    return res.json(data);
  }
}

export default new DataController();
