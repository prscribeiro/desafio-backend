import Sequelize, { Model } from 'sequelize';

class Member extends Model {
  static init(sequelize) {
    super.init(
      {
        member_name: Sequelize.STRING,
        birthday: Sequelize.STRING,
        pin_code: Sequelize.INTEGER,
      },
      {
        sequelize,
      }
    );

    return this;
  }

  static associate(models) {
    this.belongsTo(models.Employer, {
      foreignKey: 'employer_id',
      as: 'employers',
    });
  }
}

export default Member;
