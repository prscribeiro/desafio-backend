import Sequelize, { Model } from 'sequelize';

class Employer extends Model {
  static init(sequelize) {
    super.init(
      {
        employer_name: Sequelize.STRING,
        employer_code: Sequelize.STRING,
        thumbnail: Sequelize.STRING,
      },
      {
        sequelize,
      }
    );

    return this;
  }
}

export default Employer;
