import Sequelize, { Model } from 'sequelize';

class License extends Model {
  static init(sequelize) {
    super.init(
      {
        initial_date: Sequelize.STRING,
        final_date: Sequelize.STRING,
        time: Sequelize.INTEGER,
      },
      {
        sequelize,
      }
    );

    return this;
  }

  static associate(models) {
    this.belongsTo(models.Member, { foreignKey: 'member_id', as: 'member' });
  }
}

export default License;
