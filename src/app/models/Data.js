import Sequelize, { Model } from 'sequelize';

class Data extends Model {
  static init(sequelize) {
    super.init(
      {
        father: Sequelize.STRING,
        mother: Sequelize.STRING,
      },
      {
        sequelize,
      }
    );

    return this;
  }

  static associate(models) {
    this.belongsTo(models.Member, { foreignKey: 'member_id', as: 'member' });
  }
}

export default Data;
