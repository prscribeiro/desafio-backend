import jwt from 'jsonwebtoken';
import { promisify } from 'util';
import authConfig from '../../config/auth';

export default async (req, res, next) => {
  const authHeader = req.headers.authorization;

  if (!authHeader) {
    return res.status(401).json({ error: 'Authentication token not provided' });
  }

  // Destructuring to get only the token and not the Bearer
  const [, token] = authHeader.split(' ');

  try {
    const decoded = await promisify(jwt.verify)(token, authConfig.secret);

    // Get the id of the authenticated user
    req.userId = decoded.id;
  } catch (err) {
    return res.status(401).json({ error: 'Invalid Token' });
  }

  return next();
};
