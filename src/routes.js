import Router from 'express';
import UserController from './app/controllers/UserController';
import SessionController from './app/controllers/SessionController';
import EmployerController from './app/controllers/EmployerController';
import MemberController from './app/controllers/MemberController';
import DataController from './app/controllers/DataController';
import LicenseController from './app/controllers/LicenseController';

import authMiddleware from './app/middlewares/auth';

const routes = new Router();

routes.post('/users', UserController.store);
routes.post('/sessions', SessionController.store);

routes.use(authMiddleware);

routes.post('/employers', EmployerController.store);
routes.get('/employers/:employerId', EmployerController.index);
routes.post('/members', MemberController.store);
routes.get('/members/:memberId', MemberController.index);
routes.post('/data', DataController.store);
routes.post('/submit-medical-licenses', LicenseController.store);
routes.get('/medical-licenses/', LicenseController.index);

export default routes;
