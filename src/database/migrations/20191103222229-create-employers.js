module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('employers', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      employer_name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      employer_code: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      thumbnail: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      // user_id: {
      //   type: Sequelize.INTEGER,
      //   references: { model: 'users', key: 'id' },
      //   onUpdate: 'CASCADE',
      //   onDelete: 'SET NULL',
      //   allowNull: true,
      // },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
    });
  },

  down: queryInterface => {
    return queryInterface.dropTable('employers');
  },
};
