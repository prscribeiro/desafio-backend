import Sequelize from 'sequelize';
import databaseConfig from '../config/database';
import User from '../app/models/User';
import Employer from '../app/models/Employer';
import Member from '../app/models/Member';
import Data from '../app/models/Data';
import License from '../app/models/License';

/**
 * MODEL LOADER
 */

// Create an array with the models
const models = [User, Employer, Member, Data, License];

class Database {
  constructor() {
    this.init();
  }

  init() {
    // Connecting to the DB
    this.connection = new Sequelize(databaseConfig);
    // Map through the models array and calling the init method of each
    models
      .map(model => model.init(this.connection))
      .map(model => model.associate && model.associate(this.connection.models));
  }
}
export default new Database();
